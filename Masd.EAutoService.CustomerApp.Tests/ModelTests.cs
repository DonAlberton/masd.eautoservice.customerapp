﻿namespace Masd.EAutoService.CustomerApp.Tests
{
    using Masd.EAutoService.CustomerApp.Model;
    using Masd.EAutoService.CustomerApp.Utilities;
    using Microsoft.VisualStudio.TestTools.UnitTesting;



    [TestClass]
    public class ModelTests
    {
        [TestMethod]
        public void LoadServiceTask_ReadFromServiceArray_BothIdsMatches()
        {
            IModel model = new Model(new EmptyEventDispatcher());
            int checkedId = 2;
            model.SearchId = checkedId;

            model.LoadService();

            int expectedCount = 2;

            int actualCount = model.ServiceList[0].ServiceId;

            Assert.AreEqual(expectedCount, actualCount, $"Service id should be {expectedCount} and not {actualCount}");
        }

        [TestMethod]
        public void LoadServicesTask_CheckArraySize_ArraySizeMatch()
        {
            IModel model = new Model(new EmptyEventDispatcher());
            model.LoadServices();

            int expectedSize = 4;
            int actualSize = model.ServiceList.Count;

            Assert.AreEqual(expectedSize, actualSize, $"Service array size should be {expectedSize} and not {actualSize}");
        }

        [TestMethod]
        public void LoadOrdersTask_CheckServiceArrayFromOrder_ArraySizeMatch()
        {
            IModel model = new Model(new EmptyEventDispatcher());
            model.SearchId = 1;
            model.LoadOrders();

            int expectedSize = 2;
            int actualSize = model.OrderList[0].ServiceList.Count;

            Assert.AreEqual(expectedSize, actualSize, $"Service array from customer with id {model.SearchId} should be {expectedSize} and not {actualSize}");
        }

    }
}
