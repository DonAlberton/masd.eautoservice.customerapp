FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
WORKDIR /src

EXPOSE 80

COPY . .

RUN dotnet restore

RUN dotnet build "Masd.EAutoService.CustomerApp.BlazorServer" -c Debug -o /app/build

FROM build AS publish

RUN dotnet publish "Masd.EAutoService.CustomerApp.BlazorServer/Masd.EAutoService.CustomerApp.BlazorServer.csproj" -c Debug -o /app/publish

FROM base AS final
WORKDIR /app


COPY --from=publish /app/publish .

ENTRYPOINT ["dotnet", "Masd.EAutoService.CustomerApp.BlazorServer.dll"]
