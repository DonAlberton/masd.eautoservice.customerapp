﻿using System.Threading.Tasks;

using System.ComponentModel;
using System.Windows.Input;

using Masd.EAutoService.CustomerApp.Model;

namespace Masd.EAutoService.CustomerApp.Controller
{
    public interface IController : INotifyPropertyChanged
    {
        IModel Model { get; }

        ApplicationState CurrentState { get; }

        ICommand SearchServicesCommand { get; }

        ICommand SearchServiceCommand { get; }

        ICommand SearchOrdersCommand { get; }

        ICommand ShowListCommand { get; }

        ICommand AddServicesCommand { get; }

        ICommand RemoveServicesCommand { get; }

        ICommand SendOrderCommand { get; }

        Task SearchServicesAsync();

        Task SearchServiceAsync();

        Task SearchOrdersAsync();

        Task AddServicesAsync();

        Task RemoveServicesAsync();

        Task ShowListAsync();

        Task SendOrderAsync();
    }
}
