﻿using Masd.EAutoService.CustomerApp.Model;
using Masd.EAutoService.CustomerApp.Utilities;

namespace Masd.EAutoService.CustomerApp.Controller
{
    public partial class Controller : PropertyContainerBase, IController
    {
        public IModel Model { get; private set; }

        public Controller(IEventDispatcher dispatcher, IModel model) : base(dispatcher)
        {
            this.Model = model;
           
            this.SearchServicesCommand = new ControllerCommand(this.SearchServices);

            this.SearchServiceCommand = new ControllerCommand(this.SearchService);

            this.SearchOrdersCommand = new ControllerCommand(this.SearchOrders);

            this.RemoveServicesCommand = new ControllerCommand(this.RemoveServices);

            this.AddServicesCommand = new ControllerCommand(this.AddServices);

            this.SendOrderCommand = new ControllerCommand(this.SendOrder);

            this.ShowListCommand = new ControllerCommand(this.ShowList);
        }
    }
}
