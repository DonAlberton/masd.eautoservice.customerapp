﻿using System.Windows.Input;

namespace Masd.EAutoService.CustomerApp.Controller
{
    public partial class Controller : IController
    {
        public ApplicationState CurrentState
        {
            get { return this.currentState; }
            set
            {
                this.currentState = value;

                this.RaisePropertyChanged("CurrentState");
            }
        }
        private ApplicationState currentState = ApplicationState.List;

        public ICommand SearchNodesCommand { get; private set; }

        public ICommand SearchServicesCommand { get; private set; }

        public ICommand SearchServiceCommand { get; private set; }

        public ICommand SearchOrdersCommand { get; private set; }

        public ICommand ShowListCommand { get; private set; }

        public ICommand ShowMapCommand { get; private set; }

        public ICommand RemoveServicesCommand { get; private set; }
        public ICommand AddServicesCommand { get; private set; }

        public ICommand SendOrderCommand { get; private set; }

        public async Task SearchServicesAsync()
        {
            await Task.Run(() => this.SearchServices());
        }

        public async Task SearchServiceAsync()
        {
            await Task.Run(() => this.SearchService());
        }

        public async Task SearchOrdersAsync()
        {
            await Task.Run(() => this.SearchOrders());
        }

        public async Task RemoveServicesAsync()
        {
            await Task.Run(() => this.RemoveServices());
        }

        public async Task AddServicesAsync()
        {
            await Task.Run(() => this.AddServices());
        }

        public async Task SendOrderAsync()
        {
            await Task.Run(() => this.SendOrder());
        }

        public async Task ShowListAsync()
        {
            await Task.Run(() => this.ShowList());
        }

        private void SearchServices()
        {
            this.Model.LoadServices();
        }
        private void SearchService()
        {
            this.Model.LoadService();
        }
        private void SearchOrders()
        {
            this.Model.LoadOrders();
        }

        private void RemoveServices()
        {
            this.Model.RemoveServicesFromBasket();
        }

        private void AddServices()
        {
            this.Model.AddServicesToBasket();
        }

        private void SendOrder()
        {
            this.Model.ConfirmOrder();
        }

        private void ShowList()
        {
            switch (this.CurrentState)
            {
                case ApplicationState.List:
                    break;

                default:
                    this.CurrentState = ApplicationState.List;
                    break;
            }
        }
    }
}
