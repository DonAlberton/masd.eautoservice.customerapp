﻿using Masd.EAutoService.CustomerApp.Rest.Model;
using System.ComponentModel;

namespace Masd.EAutoService.CustomerApp.Model
{
    public partial class Model : IData
    {
        public event PropertyChangedEventHandler? PropertyChanged;


        public int SearchId
        {
            get { return this.searchId; }
            set
            {
                this.searchId = value;

                this.RaisePropertyChanged("SearchId");
            }
        }
        private int searchId;

        public IList<ServiceDTO> SelectedServices
        {
            get { return this.selectedServices; }
            set
            {
                this.selectedServices = value;

                this.RaisePropertyChanged("SelectedServices");
            }
        }
        private IList<ServiceDTO> selectedServices;

        public IList<ServiceDTO> SelectedServicesBasket
        {
            get { return this.selectedServicesBasket; }
            set
            {
                this.selectedServicesBasket = value;

                this.RaisePropertyChanged("SelectedServices");
            }
        }
        private IList<ServiceDTO> selectedServicesBasket;

        public ServiceDTO SelectedService
        {
            get { return this.selectedService; }
            set
            {
                this.selectedService = value;

                this.RaisePropertyChanged("SelectedServices");
            }
        }
        private ServiceDTO selectedService;


        public List<ServiceDTO> ServiceList
        {
            get { return this.serviceList; }
            private set
            {
                this.serviceList = value;

                this.RaisePropertyChanged("ServiceList");
            }
        }
        private List<ServiceDTO> serviceList = new List<ServiceDTO>();




        public ServiceDTO SelectedServiceBasket
        {
            get { return this.selectedServiceBasket; }
            set
            {
                this.selectedServiceBasket = value;

                this.RaisePropertyChanged("SelectedServiceBasket");
            }
        }
        private ServiceDTO selectedServiceBasket;

        public List<ServiceDTO> SelectedServiceList
        {
            get { return this.selectedServiceList; }
            private set
            {
                this.selectedServiceList = value;

                this.RaisePropertyChanged("SelectedServiceList");
            }
        }
        private List<ServiceDTO> selectedServiceList = new List<ServiceDTO>();



        public List<OrderAppDTO> OrderList
        {
            get { return this.orderList; }
            private set
            {
                this.orderList = value;

                this.RaisePropertyChanged("Service");
            }
        }
        private List<OrderAppDTO> orderList;

        public OrderAppDTO SelectedOrder
        {
            get { return this.selectedOrder; }
            set
            {
                this.selectedOrder = value;

                this.RaisePropertyChanged("SelectedService");
            }
        }
        private OrderAppDTO selectedOrder;

        public string CustomerName
        {
            get { return this.customerName; }
            set
            {
                this.customerName = value;

                this.RaisePropertyChanged("CustomerName");
            }
        }
        private string customerName;

        public string CustomerSurname
        {
            get { return this.customerSurname; }
            set
            {
                this.customerSurname = value;

                this.RaisePropertyChanged("CustomerSurname");
            }
        }
        private string customerSurname;

        public double TotalCost
        {
            get { return this.totalCost; }
            set
            {
                this.totalCost = value;

                this.RaisePropertyChanged("TotalCost");
            }
        }
        private double totalCost;
        public int OrderId
        {
            get { return this.orderId; }
            set
            {
                this.orderId = value;

                this.RaisePropertyChanged("TotalCost");
            }
        }
        private int orderId;
    }

}
