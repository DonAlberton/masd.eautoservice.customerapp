﻿using Masd.EAutoService.CustomerApp.Rest.Client;
using Masd.EAutoService.CustomerApp.Rest.Model;

namespace Masd.EAutoService.CustomerApp.Model
{

    public partial class Model : IOperations
    {
#if DEBUG
        private IOrdersAppService orderClient = new OrdersAppServiceClient();
#else
        private IOrdersAppService orderClient = new MockOrdersAppServiceClient();
#endif
        public void LoadServices()
        {
            this.LoadServicesTask();

        }

        public void LoadService()
        {
            this.LoadServiceTask();

        }

        public void LoadOrders()
        {
            this.LoadOrdersTask();

        }
        public void AddServicesToBasket()
        {
            this.AddServicesToBasketTask();

        }

        public void RemoveServicesFromBasket()
        {
            this.RemoveServicesFromBasketTask();

        }

        public void ConfirmOrder()
        {
            this.ConfirmOrderTask();

        }
        private void LoadServicesTask()
        {
            try
            {
                ServiceDTO[] services = orderClient.GetServices();

                this.ServiceList = services.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void LoadServiceTask()
        {
            try
            {
                ServiceDTO[] service = new ServiceDTO[] { orderClient.GetService(this.searchId) };
                if (service[0] == null)
                {
                    throw new Exception("Invalid Id");
                }
                this.ServiceList = service.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void AddServicesToBasketTask()
        {
            try
            {
                if (SelectedServices.ToList() == null)
                {
                    throw new Exception("Services not selected");
                }
                else if (SelectedServices.ToList() == SelectedServiceList)
                {
                    throw new Exception("Nothing to add");
                }
                else if (SelectedServiceList == serviceList)
                {
                    throw new Exception("All services already added");
                }
                else
                {
                    this.SelectedServiceList = Enumerable.Union(this.SelectedServiceList, SelectedServices.ToList()).ToList();
                    this.TotalCost = 0;
                    foreach (ServiceDTO service in SelectedServiceList)
                    {
                        this.TotalCost += service.Price;
                    }
                }     
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void RemoveServicesFromBasketTask()
        {
            try
            {
                if (SelectedServicesBasket.ToList() == null)
                {
                    throw new Exception("Services not selected");
                }
                else
                {
                    this.TotalCost = 0;
                    foreach (ServiceDTO selectedService in SelectedServicesBasket.ToList())
                    {
                        this.SelectedServiceList.Remove(selectedService);
                    }
                    foreach(ServiceDTO service in SelectedServiceList)
                    {
                        this.TotalCost += service.Price;
                    }
                    SelectedServiceList.ToList();
                    

                }              
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        private void ConfirmOrderTask()
        {

            try
            {
                if(CustomerSurname == null || CustomerName == null || SelectedServiceList == null) 
                { 
                    throw new Exception("Invalid name or surname");
                }
                else 
                {
                    List<int> serviceList = new List<int> { };
                    for (int x = 0; x < SelectedServiceList.Count; x++)
                    {
                        serviceList.Add(SelectedServiceList[x].ServiceId);
                    }

                    orderClient.MakeOrder(serviceList, CustomerName, CustomerSurname);
                }               
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        private void LoadOrdersTask()
        {
            try
            {
                OrderAppDTO[] orders = new OrderAppDTO[] { orderClient.GetOrder(this.searchId) };
                if (orders[0] == null)
                {
                    throw new Exception("Invalid Id");
                }
                this.OrderList = orders.ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
