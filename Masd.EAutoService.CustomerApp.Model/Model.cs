﻿using Masd.EAutoService.CustomerApp.Utilities;

namespace Masd.EAutoService.CustomerApp.Model
{
  public partial class Model : PropertyContainerBase, IModel
  {
    public Model( IEventDispatcher dispatcher ) : base( dispatcher )
    {
    }
  }
}
