﻿namespace Masd.EAutoService.CustomerApp.Model
{
    public interface IOperations
    {
        void LoadServices();

        void LoadService();

        void LoadOrders();

        void AddServicesToBasket();

        void RemoveServicesFromBasket();

        void ConfirmOrder();
    }
}
