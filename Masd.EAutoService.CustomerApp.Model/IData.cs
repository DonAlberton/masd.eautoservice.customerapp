﻿using Masd.EAutoService.CustomerApp.Rest.Model;
using System.ComponentModel;

namespace Masd.EAutoService.CustomerApp.Model
{
    public interface IData : INotifyPropertyChanged
    {
        List<ServiceDTO> SelectedServiceList { get; }
        ServiceDTO SelectedServiceBasket { get; set; }
               
        ServiceDTO SelectedService { get; set; }

        IList<ServiceDTO> SelectedServices { get; set; }

        IList<ServiceDTO> SelectedServicesBasket { get; set; }
        List<ServiceDTO> ServiceList { get; }

        List<OrderAppDTO> OrderList { get; }
        OrderAppDTO SelectedOrder { get; set; }

        string CustomerName { get; set; }
        string CustomerSurname { get; set; }

        int OrderId { get; set; }

        int SearchId { get; set; }
        double TotalCost { get; set; }

       
    }
}
