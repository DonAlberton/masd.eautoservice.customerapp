﻿namespace Masd.EAutoService.CustomerApp.Rest.Model
{
    public interface IMechanicsDataService
    {
        public MechanicDTO[] GetMechanics();
        public MechanicDTO GetMechanic(int id);
    }
}