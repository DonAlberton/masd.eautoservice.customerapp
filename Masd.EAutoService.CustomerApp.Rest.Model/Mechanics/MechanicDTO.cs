﻿namespace Masd.EAutoService.CustomerApp.Rest.Model
{
    public class MechanicDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Surname { get; set; }

    }
}
