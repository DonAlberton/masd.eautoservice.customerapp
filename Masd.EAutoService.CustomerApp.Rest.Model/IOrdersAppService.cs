﻿namespace Masd.EAutoService.CustomerApp.Rest.Model
{
    public interface IOrdersAppService
    {
        public ServiceDTO GetService(int id);
        public ServiceDTO[] GetServices();
        public void MakeOrder(List<int>? serviceList, string name, string surname);
        public OrderAppDTO GetOrder(int id);
    }
}
