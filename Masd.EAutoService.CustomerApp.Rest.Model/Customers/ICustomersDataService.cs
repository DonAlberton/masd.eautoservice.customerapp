﻿namespace Masd.EAutoService.CustomerApp.Rest.Model
{
    public interface ICustomersDataService
    {
        CustomerDTO GetCustomer(int searchText);
        CustomerDTO[] GetCustomers();

        public void AddCustomer(string customerName, string customerSurname);
    }
}

