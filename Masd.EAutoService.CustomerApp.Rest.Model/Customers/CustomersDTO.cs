﻿namespace Masd.EAutoService.CustomerApp.Rest.Model
{
    public class CustomerDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
    }
}
