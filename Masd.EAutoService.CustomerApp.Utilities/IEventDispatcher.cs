﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Masd.EAutoService.CustomerApp.Utilities
{
  public interface IEventDispatcher
  {
    void Dispatch( Action action );
  }
}