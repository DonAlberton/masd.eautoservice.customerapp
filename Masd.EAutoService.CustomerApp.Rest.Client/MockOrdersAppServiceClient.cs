﻿using Masd.EAutoService.CustomerApp.Rest.Model;

namespace Masd.EAutoService.CustomerApp.Rest.Client
{
    public class MockOrdersAppServiceClient : IOrdersAppService
    {


        public static OrderAppDTO[] ordersDTO = new OrderAppDTO[] { 
            new OrderAppDTO { Id=1, MechanicName="Robert", MechanicSurname="Winnicki", CustomerName="Stefan", CustomerSurname="Wyszynski", IsFinished=true, ServiceList = new List<String>{"Oil change", "Tireschange" } },
            new OrderAppDTO { Id=2, MechanicName="Orlando", MechanicSurname="Merloni", CustomerName="Adam", CustomerSurname="Rybalko", IsFinished=true, ServiceList = new List<string>{"Tireschange"} }
        };



        public static ServiceDTO[] servicesDTO = new ServiceDTO[] { new ServiceDTO { ServiceId = 1, Name = "Oil change", Price = 20.00 }, 
                                                                    new ServiceDTO { ServiceId = 2, Name = "Filter Change", Price = 15.25 }, 
                                                                    new ServiceDTO { ServiceId = 3, Name = "Breaks Change", Price = 19.99 }, 
                                                                    new ServiceDTO { ServiceId = 4, Name = "Tires change", Price = 9.99 } };



        public ServiceDTO[] GetServices()
        {
            return servicesDTO;
        }

        public ServiceDTO GetService(int id)
        {
            return servicesDTO.FirstOrDefault(m => m.ServiceId == id)!;
        }

        public void MakeOrder(List<int> serviceList, string name, string surname)
        {

        }

        public int GetCustomerId()
        {
            int Id1 = 1;
            return Id1;
        }
        public int GetMechanicId()
        {
            int Id1 = 1;
            return Id1;
        }
        public int GetOrderId()
        {
            int Id1 = 1;
            return Id1;
        }


        public OrderAppDTO GetOrder(int id)
        {
            return ordersDTO.FirstOrDefault(m => m.Id == id)!;
        }

    }
}
