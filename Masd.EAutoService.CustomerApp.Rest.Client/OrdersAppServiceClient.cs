﻿using Masd.EAutoService.CustomerApp.Rest.Model;
using System.Globalization;
using System.Net.Http.Headers;

using System.Text;
using System.Text.Json;


namespace Masd.EAutoService.CustomerApp.Rest.Client
{
    public class OrdersAppServiceClient : IOrdersAppService
    {
        private static readonly HttpClient httpClient = new HttpClient();
        
        /*private readonly int port = 8092;
        private readonly string ipAddress = "localhost";*/

        private readonly int port = 80;
        private readonly string ipAddress = "192.168.50.112";


        public ServiceDTO[] GetServices()
        {
            string webServiceUrl = String.Format("http://{0}:{1}/OrdersApp/GetServices", ipAddress, port);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO[] servicesData = ConvertJson(jsonResponseContent);

            return servicesData;
        }
        public ServiceDTO GetService(int id)
        {
            string webServiceUrl = String.Format("http://{0}:{1}/OrdersApp/GetService?id={2}", ipAddress, port, id);

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            ServiceDTO servicesData = ConvertJsonObject(jsonResponseContent);

            return servicesData;
        }

        public void MakeOrder(List<int> serviceList, string name, string surname)
        {
            string webServiceUrl = String.Format("http://{0}:{1}/OrdersApp/MakeOrder?name={2}&surname={3}", ipAddress, port, name, surname);
            String listParameter = JsonSerializer.Serialize(serviceList);
            StringContent content = new StringContent(listParameter, Encoding.UTF8, "application/json");

            Task<string> webServiceCall2 = CallWebService3(HttpMethod.Post, webServiceUrl, content);

            webServiceCall2.Wait();

        }


        public OrderAppDTO GetOrder(int id)
        {
            string webServiceUrl = $"http://{ipAddress}:{port}/OrdersApp/GetOrder?orderId={id}";

            Task<string> webServiceCall = CallWebService(HttpMethod.Get, webServiceUrl);

            webServiceCall.Wait();

            string jsonResponseContent = webServiceCall.Result;

            OrderAppDTO mechanicsData = ConvertJsonObjectOrder(jsonResponseContent);

            return mechanicsData;
        }


        private async Task<string> CallWebService(HttpMethod httpMethod, string webServiceUrl)
        {
            HttpRequestMessage httpRequestMessage = new HttpRequestMessage(httpMethod, webServiceUrl);

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");

            HttpResponseMessage httpResponseMessage = await httpClient.SendAsync(httpRequestMessage);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private async Task<string> CallWebService3(HttpMethod httpMethod, string webServiceUrl, StringContent content)
        {

            httpClient.DefaultRequestHeaders.Add("Accept", "application/json");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));

            HttpResponseMessage httpResponseMessage = await httpClient.PostAsync(webServiceUrl, content);

            httpResponseMessage.EnsureSuccessStatusCode();

            string httpResponseContent = await httpResponseMessage.Content.ReadAsStringAsync();

            return httpResponseContent;
        }

        private ServiceDTO[] ConvertJson(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            ServiceDTO[] servicesData = JsonSerializer.Deserialize<ServiceDTO[]>(json, options)!;

            return servicesData;
        }

        private ServiceDTO ConvertJsonObject(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            ServiceDTO servicesData = JsonSerializer.Deserialize<ServiceDTO>(json, options)!;

            return servicesData;
        }

        private OrderAppDTO ConvertJsonObjectOrder(string json)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };
            OrderAppDTO mechanicsData = JsonSerializer.Deserialize<OrderAppDTO>(json, options);

            return mechanicsData;
        }

    }
}
